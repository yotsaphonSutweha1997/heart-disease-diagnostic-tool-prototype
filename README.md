# Heart-Disease-Diagnostic-tool-prototype

# Steps:
	Run command: python server.py
	navigate to http://localhost:8000/diagnosis-results to use diagnostic functionality
	navigate to http://localhost:8000/visualization to see the mock up for patient's condition visualization
	
# Final version 
	
	Frontend: https://github.com/yotsaphonSutweha/H.D.D.T-Frontend
	Backend: https://github.com/yotsaphonSutweha/H.D.D.T