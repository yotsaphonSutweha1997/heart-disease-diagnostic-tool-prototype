from flask import Flask
from flask import render_template, request, url_for
from flask_cors import CORS
from random import seed
from random import randrange
from csv import reader 
from math import *
import numpy as np
import perceptron 
import knn
app = Flask(__name__, static_url_path='/static')
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0

@app.route('/')
@app.route('/<name>')
def index(name="Human"):
    return render_template("index.html", name=name)

@app.route('/dashboard')
def dashboard():
    return render_template("dashboard.html", myList = [0, 1, 2, 3, 4, 5])

@app.route('/awaiting')
def awaiting():
    return render_template("awaiting.html", myList = [0, 1, 2, 3, 4, 5])

@app.route('/view')
def view():
    return render_template("view.html")

@app.route('/edit')
def edit():
    return render_template("edit.html")


@app.route('/diagnosis-results', methods = ['GET', 'POST'])
def diagnose():
    if request.method == 'POST':
        age = request.form.get('age')
        sex = request.form.get('sex')
        cp = request.form.get('cp')
        trestbps = request.form.get('trestbps')
        chol = request.form.get('chol')
        fbs = request.form.get('fbs')
        restecg = request.form.get('restecg')
        thalach = request.form.get('thalach')
        exang = request.form.get('exang')
        oldpeak = request.form.get('oldpeak')
        slope = request.form.get('slope')
        ca = request.form.get('ca')
        thal = request.form.get('thal')

        diagnosticResult = ''
        perceptronPredictedText = ''
        knnPredictedText = ''
        patientConditions = [float(age), float(sex), float(cp), float(trestbps), float(chol), float(fbs), float(restecg), float(thalach), float(exang), float(oldpeak), float(slope), float(ca), float(thal)]
        print(patientConditions)
        perceptronAccuracy, perceptronPredicted, knnAccuracy, knnPredicted = heartDiseaseDiagnosis(patientConditions)
        perceptronAccuracy = round(perceptronAccuracy, 2)
        knnAccuracy = round(knnAccuracy, 2)

        if int(perceptronPredicted) == 1 and int(knnPredicted) == 1:
            diagnosticResult = 'Patient is diagnosed with heart disease'
        elif int(perceptronPredicted) == 0 and int(knnPredicted) == 0:
            diagnosticResult = 'Patient is not diagnosed with heart disease'
        
        if int(perceptronPredicted) == 1:
            perceptronPredictedText = 'Positive'

        if int(knnPredicted) == 1:
            knnPredictedText = 'Positive'

        if int(perceptronPredicted) == 0:
            perceptronPredictedText = 'Negative'

        if int(knnPredicted) == 0:
            knnPredictedText = 'Negative'

        return render_template("results.html", perceptronAccuracy=perceptronAccuracy, knnAccuracy=knnAccuracy, perceptronPredicted=perceptronPredictedText, knnPredicted=knnPredictedText, diagnosticResult=diagnosticResult)
    return render_template("patient-conditions.html")

@app.route('/visualization')
def visualization():
     return render_template("visualization.html")

def loadCsv(filename):
    dataset = list()
    with open(filename, 'r') as file:
        csvReader = reader(file)
        for row in csvReader:
            if not row:
                continue
            dataset.append(row)
    return dataset

def trainTestSplit(dataset, trainSplit = 0.75):
    train = list()
    trainSize = trainSplit * len(dataset)
    datasetCopy = list(dataset)
    while len(train) < trainSize:
        index = randrange(len(datasetCopy))
        train.append(datasetCopy.pop(index))
    return train, datasetCopy

def convertStringColumnToFloat(hd, column):
    for row in hd:
        row[column] = float(row[column].strip()) 

def convertStringColumnToInt(dataset, column):
    classValues = [row[column] for row in dataset]
    uniqueValues = set(classValues)
    lookup = dict()
    for i, value in enumerate(uniqueValues):
        lookup[value] = i
    for row in dataset:
        row[column] = lookup[row[column]]
    return lookup 

def accuracyMetric(actual, predicted):
    correct = 0
    for i in range(len(actual)):
        if actual[i] == predicted[i]:
            correct += 1
    return correct / float(len(actual)) * 100.0

def dataPreprocessing(filename):
    data = loadCsv(filename)
    hd = data[1: len(data)]
    for i in range(len(hd[0])-1):
        convertStringColumnToFloat(hd, i)
    convertStringColumnToInt(hd, len(hd[0])-1)
    dataTrain, dataTest = trainTestSplit(hd)
    return dataTrain, dataTest

def perceptronModel(dataTrain, dataTest, patientCondition, actualResults):
    lRate = 0.06
    nEpoch = 1000 
    perceptronPredictions, patientDiagnosis = perceptron.buildPerceptron(dataTrain, dataTest, patientCondition, lRate, nEpoch)
    perceptronAccuracy = perceptronEvaluation(perceptronPredictions, actualResults)
    return perceptronAccuracy, patientDiagnosis

def knnModel(dataTrain, dataTest, patientCondition, actualResults):
    knnPredicts = list()
    for i in range(len(dataTest)):
        knnPrediction = knn.buildKnn(dataTrain, dataTest[i], 4)
        knnPredicts.append(knnPrediction)
    knnAccuracy = accuracyMetric(actualResults, knnPredicts)
    patientDiagnosis = knn.buildKnn(dataTrain, patientCondition, 4)
    return knnAccuracy, patientDiagnosis

def perceptronEvaluation(perceptronPredictions, actualResults):
    intValsPerceptonPrecitions = list()
    for i in perceptronPredictions:
        intValsPerceptonPrecitions.append(int(i))
    perceptronAccuracy = accuracyMetric(actualResults, intValsPerceptonPrecitions)
    return perceptronAccuracy
# take a look closely at how each row is being handled by the models
def heartDiseaseDiagnosis(patientConditions):
    trainDataset, testDataset = dataPreprocessing("clevelandV4.csv")
    test = testDataset[len(testDataset)-1]
    #realValue = patientCondition[-1]
    print("Y: {0}".format(test))
    print("X: {0}".format(patientConditions))
    actualResults = list()
    for row in testDataset:
        actualResults.append(row[-1])
    perceptronAccuracy, patientDiagnosisPerceptron = perceptronModel(trainDataset, testDataset, patientConditions, actualResults)
    knnAccuracy, patientDiagnosisKNN = knnModel(trainDataset, testDataset, patientConditions, actualResults)
    return perceptronAccuracy, patientDiagnosisPerceptron, knnAccuracy, patientDiagnosisKNN, 

mockPatients = [
    {   "Name": "John Doe",
        "Contact info": "086-841-9088",
        "Next of Kin": "Janny Doe",
        "Status": "Awaiting for diagnosis"
    },
    {
        "Name": "Lauren Johnson",
        "Contact info": "086-841-4000",
        "Next of Kin": "Jake Johnson",
        "Status": "Awaiting for diagnosis"
    },
    {
        "Name": "Kevin Brady",
        "Contact info": "084-222-9088",
        "Next of Kin": "John Brady",
        "Status": "Awaiting for diagnosis"
    },
    {
        "Name": "Frank Murphy",
        "Contact info": "086-444-5555",
        "Next of Kin": "Janny Murphy",
        "Status": "Awaiting for diagnosis"
    },
    {   "Name": "Janna Hugh",
        "Contact info": "085-555-9988",
        "Next of Kin": "Janny Hughs",
        "Status": "Awaiting for diagnosis"},
    {
        "Name": "Helen Smith",
        "Contact info": "081-222-9088",
        "Next of Kin": "John Smith",
        "Status": "Awaiting for diagnosis"
    },
]


app.run(debug=True, port=8000, host='0.0.0.0')


# index 301 for testing: 57,0,2,130,236,0,2,174,0,0,2,1,1,1